
import math
import rules as rl
import matplotlib.pyplot as plt
import numpy as np
import random as rand
import pandas as pd
import itertools
import betas_and_experts as be


# IDEAS
# Bias for dimensions affects how much each dimension is updated
# Plot spans separately
# Effect of n_rule_violations?
# Use actual trials of humans
# Add more experts: C+S, C+F, S+F
# Adaptively learn how to weigh each expert, depending on how good its predictions are


# Rules A ('match'), B ('span'), C ('1==2 & 2!=3), D (1==3&2!=3), E (2==3&1!=2)
# Define rules
rule_names = ['A', 'B', 'X']
colors = ['r', 'g', 'b']
shapes = ['t', 's', 'm']
fills = ['l', 'd', 'f']
n_trials = 11 * 12

# Get all possible tuples and labels as SET or noSET
csf = ['Color', 'Shape', 'Fill']
all_tuples = pd.DataFrame(columns=csf)
all_tuples['Color'] = rl.get_all_tuples(colors)
all_tuples['Shape'] = rl.get_all_tuples(shapes)
all_tuples['Fill'] = rl.get_all_tuples(fills)
all_tuples['SET'] = 'noSET'
color_SET_tuples = rl.get_all_tuples_for_a_rule('A') + rl.get_all_tuples_for_a_rule('B')
all_tuples_SET_indexes = [tpl in color_SET_tuples for tpl in all_tuples['Color']]
all_tuples.loc[all_tuples_SET_indexes, 'SET'] = 'SET'

# Get the five possible rules for each dimension (match, span, C, D, E)
rules = pd.DataFrame(columns=csf)
rules['Color'] = be.get_beta_rules(colors, rule_names=rule_names)[0]
rules['Shape'] = be.get_beta_rules(shapes, rule_names=rule_names)[0]
rules['Fill'] = be.get_beta_rules(fills, rule_names=rule_names)[0]
rules['RuleName'] = be.get_beta_rules(colors, rule_names=rule_names)[1]

# Simulations
n_simulations = 500

simulation_columns = ['SimulationID', 'alpha_patterns', 'alpha_rules', 'TrialID', 'span', 'rules_broken',
                      'Expert', 'Prediction', 'Likelihood', 'ACC']
simulations = pd.DataFrame(columns=simulation_columns)
csf_combos = [[csf[0]], [csf[1]], [csf[2]]] + [[csf[i[0]], csf[i[1]]] for i in itertools.combinations(range(3), 2)] + [csf]
post = '_Post'
colnames = [''.join([CSF, post]) for CSF in csf]
colnames += [''.join([CSF[0], CSF[1], post]) for CSF in list(itertools.combinations(csf, 2))]
colnames += [''.join([CSF[0], CSF[1], CSF[2], post]) for CSF in list(itertools.combinations(csf, 3))]
# Simulate random data (50% SET, 50% noSET)
# trials = be.simulate_random_data(all_tuples, column_names=csf, n_rows=np.arange(250))
trials_ordered = pd.read_csv('C:/Users/maria/MEGAsync/Berkeley/learnSET/ChurchCode/learnSETtrials.csv')
for simulation in range(n_simulations):

    # Draw parameters: alphas & betas
    total_patterns_s = np.array([10, 5, 5])   # [match, span, X]
    total_patterns = [be.get_alpha(1, upper, upper, upper/3) for i, upper in enumerate(total_patterns_s)]
    pattern_means = np.array([total_patterns[0] * 2/4, total_patterns[1] * 1/4, total_patterns[2] * 1/4])
    initial_alpha_patterns = np.array([be.get_alpha(1, total_patterns[i], pattern_means[i], total_patterns[i]/3)
                                       for i in range(len(total_patterns))])
    initial_beta_patterns = total_patterns - initial_alpha_patterns

    total_rules = 7 * np.ones(7)
    rule_means = [total_rules[i] * 3/4 for i in range(3)] + [total_rules[i] * 2/4 for i in range(3, 6)] + [total_rules[6] * 1/4]
    initial_alpha_rules = np.array([be.get_alpha(1, total_rules[i], rule_means[i], total_rules[i]/3)
                                    for i in range(len(total_rules))])
    initial_beta_rules = total_rules - initial_alpha_rules

    # Prepare trials dataframe
    trials = pd.DataFrame(columns=trials_ordered.columns, index=trials_ordered.index)
    rows = []
    shuffled_rows = []
    for chunk in range(0, n_trials, 11):
        row_chunk = list(range(chunk, (chunk+11)))
        rows += row_chunk
        shuffled_rows += list(np.random.permutation(row_chunk))
    trials = trials_ordered.loc[rows, :].reindex(shuffled_rows)
    trials.index = rows
    trials.loc[trials['SET'] == 'wudsy', 'SET'] = 'SET'
    trials.loc[trials['SET'] == 'non-wudsy', 'SET'] = 'noSET'
    for pos, dim in enumerate(csf):
        trials[dim] = [(S1[pos], S2[pos], S3[pos]) for S1, S2, S3 in zip(trials['Shape1'], trials['Shape2'], trials['Shape3'])]

    # Get beta posteriors for rules A, B, X for all dimensions
    for dim in csf:   # csf_combos
        initial_alpha = initial_alpha_patterns
        initial_beta = initial_beta_patterns
        alpha_col = ''.join([dim, '_Alpha'])
        beta_col = ''.join([dim, '_Beta'])
        poster_col = ''.join([dim, '_Post'])
        be.get_alpha_and_beta(trials, rules, dim,
                              alpha_col, beta_col,
                              initial_alpha, initial_beta)
        trials[poster_col] = 'NaN'
        for row in range(trials.shape[0]):
            posterior = (trials.loc[row, alpha_col] / (trials.loc[row, alpha_col] + trials.loc[row, beta_col]))
            trials.set_value(row, poster_col, posterior)

    # Plots: alpha, beta, and posterior for the dimensional experts
    Alphas_Betas = pd.DataFrame()
    for dim in csf:
        for rule_i, rule in enumerate(rule_names):
            for abp in ['Alpha', 'Beta', 'Post']:
                trial_col = ''.join([dim, '_', abp])
                new_col = ''.join([dim, abp, '_', rule])
                values = [row[rule_i] for row in trials[trial_col]]
                Alphas_Betas[new_col] = values

    # fig1 = plt.figure()
    # for plot_counter, abp in enumerate(['Alpha', 'Beta', 'Post']):
    #     abp_col_indexes = [abp in col for col in Alphas_Betas.columns.values]
    #     abp_cols = Alphas_Betas.columns[abp_col_indexes]
    #     for column in abp_cols:
    #         subfig = fig1.add_subplot(1, 3, plot_counter + 1)
    #         if '_A' in column:
    #             color = 'b'
    #         elif '_B' in column:
    #             color = 'g'
    #         elif '_X' in column:
    #             color = 'r'
    #         else:
    #             color = 'k'
    #         subfig.plot(range(len(Alphas_Betas[column])), Alphas_Betas[column], color)
    #         subfig.set_xlabel('Trial')
    #         if abp in ['Alpha', 'Beta']:
    #             subfig.set_title(abp)
    #             subfig.set_ylabel('Count')
    #             subfig.set_ylim([0, 45])
    #         else:
    #             # subfig.set_title('Association strength')
    #             subfig.set_ylabel('alpha / (alpha + beta)')
    #             subfig.set_ylim([0, 1])
    # plt.show()

    # See what the experts say in each trial
    experts = pd.DataFrame(index=range(trials.shape[0]))
    for dim in csf:   # csf_combos
        poster_col = ''.join([dim, '_Post'])
        experts[poster_col] = False
        for row in range(trials.shape[0]):
            tpl = trials.loc[row, dim]
            rule_index = rl.get_rule_from_tuple(tpl, rules, dim)
            rule_pred = ['SET' if trials.loc[row, poster_col][rule_index] > 1/3 else 'noSET'][0]
            experts.loc[row, poster_col] = rule_pred

    # Learn how to combine the experts: Get predictions for combined experts
    for dims in list(itertools.combinations(csf, 2)):  # + list(itertools.combinations(csf, 3)):
        col_name = ''.join([dims[0], dims[1], '_Post'])
        poster_col0 = ''.join([dims[0], '_Post'])
        poster_col1 = ''.join([dims[1], '_Post'])
        experts[col_name] = 'noSET'
        experts.loc[(experts[poster_col0] == 'SET') & (experts[poster_col1] == 'SET'), col_name] = 'SET'
    experts['ColorShapeFill_Post'] = 'noSET'
    experts.loc[(experts[colnames[3]] == 'SET') & (experts[colnames[4]] == 'SET') & (experts[colnames[5]] == 'SET'),
                'ColorShapeFill_Post'] = 'SET'

    # Count how often each expert makes right predictions
    experts['SET'] = trials['SET']
    experts['Alpha'] = 'NaN'
    experts['Beta'] = 'NaN'
    experts.set_value(0, 'Alpha', initial_alpha_rules)
    experts.set_value(0, 'Beta', initial_beta_rules)
    for row in range(1, experts.shape[0]):
        correct_prediction = experts.loc[row, colnames] == experts.loc[row, 'SET']
        new_alphas = np.array(experts.loc[row-1, 'Alpha'].copy() + correct_prediction)
        new_betas = np.array(experts.loc[row-1, 'Beta'].copy() + 1 - correct_prediction)
        experts.set_value(row, 'Alpha', new_alphas)
        experts.set_value(row, 'Beta', new_betas)
    posterior_cols = [''.join([colname[:-5]]) for colname in colnames]
    for col in posterior_cols:
        experts[col] = 'NaN'
    for row in range(experts.shape[0]):
        experts.set_value(row, posterior_cols,
                          experts.loc[row, 'Alpha'] / (experts.loc[row, 'Alpha'] + experts.loc[row, 'Beta']))
    #
    # # Plots: alpha, beta, and posterior for the rule experts
    # Alphas_Betas_r = pd.DataFrame()
    # for csf_counter, CSF in enumerate(csf_combos):
    #     for ab in ['Alpha', 'Beta']:
    #         new_col = ''.join(CSF + [ab])
    #         values = [row[csf_counter] for row in experts[ab]]
    #         Alphas_Betas_r[new_col] = values
    # post_colnames_r = [''.join([col, 'Post']) for col in posterior_cols]
    # Alphas_Betas_r[post_colnames_r] = experts[posterior_cols]
    #
    # fig2 = plt.figure()
    # for plot_counter, abp in enumerate(['Alpha', 'Beta', 'Post']):
    #     abp_col_indexes = [abp in col for col in Alphas_Betas_r.columns.values]
    #     abp_cols = Alphas_Betas_r.columns[abp_col_indexes]
    #     for column in abp_cols:
    #         subfig = fig2.add_subplot(1, 3, plot_counter + 1)
    #         if len(column) < 12:
    #             color = 'b'
    #         elif len(column) < 17:
    #             color = 'g'
    #         elif len(column) < 20:
    #             color = 'r'
    #         else:
    #             color = 'k'
    #         subfig.plot(range(len(Alphas_Betas_r[column])), Alphas_Betas_r[column], color)
    #         subfig.set_xlabel('Trial')
    #         if abp in ['Alpha', 'Beta']:
    #             subfig.set_title(abp)
    #             subfig.set_ylabel('Count')
    #             subfig.set_ylim([0, 120])
    #         else:
    #             # subfig.set_title('Association strength')
    #             subfig.set_ylabel('alpha / (alpha + beta)')
    #             subfig.set_ylim([0, 1])
    # plt.show()

    # Simulation: WSLS
    # Create a few simulated runs of the task
    normalized_probs = np.array(list(experts.loc[0, posterior_cols])) / np.sum(experts.loc[0, posterior_cols])
    chosen_expert = np.random.choice(posterior_cols, p=normalized_probs)
    for row in range(experts.shape[0]):
        # What's the agent's prediction? P('SET'|observed_rule)
        prediction = experts.loc[row, ''.join([chosen_expert, '_Post'])]
        # What's the correct answer?
        true_answer = experts.loc[row, 'SET']
        # Did the agent get it right?
        accuracy = [1 if prediction == true_answer else 0][0]
        # Likelihood of observing rule under current hypothesis, e.g., P(observed_rule|'SET')
        if chosen_expert in posterior_cols[:3]:
            dim_cols = chosen_expert
        elif chosen_expert in posterior_cols[3:6]:
            dim_cols = [chosen_expert[:5], chosen_expert[5:]]
        else:
            dim_cols = csf
        current_trial = trials.loc[row, csf]
        current_rules = rl.get_rule_from_tuple(current_trial, rules, csf)
        span = np.sum(np.array(current_rules) == 1)
        rules_broken = np.sum(np.array(current_rules) > 1)
        if accuracy == 1:
            likelihood = 1
        else:
            likelihood = 0
        # Save the current trial to simulations data frame
        new_row = pd.DataFrame(data=[[simulation, initial_alpha_patterns, initial_alpha_rules, row, span, rules_broken,
                                      ''.join(chosen_expert), prediction, likelihood, accuracy]],
                               columns=simulation_columns)
        simulations = simulations.append(new_row)
        # Decide whether to switch to a new hypothesis
        if rand.random() > likelihood:
            normalized_probs = np.array(list(experts.loc[row, posterior_cols])) / np.sum(experts.loc[row, posterior_cols])
            chosen_expert = np.random.choice(posterior_cols, p=normalized_probs)

simulations.to_csv('C:/Users/maria/MEGAsync/Berkeley/learnSET/ChurchCode/simulations5.csv')

# Get average ACC of simulations
av_simulations = pd.DataFrame(columns=['av_ACC', 'n_SET_rule'])
sim_ACC_colnames = []
sim_SET_rules_colnames = []
for n_sim in range(n_simulations):
    # Save ACC of this simulation into av_simualations dataframe
    sim_ACC = simulations.loc[simulations['SimulationID'] == n_sim, 'ACC']
    sim_ACC_colname = ''.join(['ACC_', str(n_sim)])
    av_simulations[sim_ACC_colname] = sim_ACC
    # Save when this simulation came up with the SET rule
    sim_SET_rules = simulations.loc[simulations['SimulationID'] == n_sim, 'Expert'] == posterior_cols[6]
    sim_SET_rules_colname = ''.join(['n_SET_rule_', str(n_sim)])
    av_simulations[sim_SET_rules_colname] = sim_SET_rules
    # Save colnames as lists
    sim_ACC_colnames += [sim_ACC_colname]
    sim_SET_rules_colnames += [sim_SET_rules_colname]
# Take ACC and n_SET_rule averages of all simulations
av_simulations['av_ACC'] = np.mean(av_simulations[sim_ACC_colnames], 1)
av_simulations['n_SET_rule'] = np.mean(av_simulations[sim_SET_rules_colnames], 1)

# Plot simulations
# ACC
fig3 = plt.figure()
for n_sim in range(n_simulations):
    ACC = av_simulations[''.join(['ACC_', str(n_sim)])]
    plt.plot(range(len(ACC)), ACC, 'y-')
av_ACC = av_simulations[''.join(['av_ACC'])]
plt.plot(range(len(av_ACC)), av_ACC, 'k-')
plt.show()
# n_SET_rules
fig4 = plt.figure()
for n_sim in range(n_simulations):
    n_SET = av_simulations[''.join(['n_SET_rule_', str(n_sim)])]
    plt.plot(range(len(n_SET)), n_SET, 'y-')
av_n_SET = av_simulations[''.join(['n_SET_rule'])]
plt.plot(range(len(av_n_SET)), av_n_SET, 'k-')
plt.xlabel('Trial')
plt.ylabel('Agents using true rule (%)')
plt.show()
