(define (SET? example)
   
   (define colors (list (first (first example)) (second (first example)) (third (first example))))
   (define shapes (list (first (second example)) (second (second example)) (third (second example))))
   (define fills (list (first (third example)) (second (third example)) (third (third example))))
   
   (define (match? feature) (if (and (equal? (first feature) (second feature))
                                     (equal? (second feature) (third feature)))
                                true false))
   (define (span? feature) (if (and (not (equal? (first feature) (second feature)))
                                    (not (equal? (first feature) (third feature)))
                                    (not (equal? (second feature) (third feature))))
                               true false))
   
   (if (and (or (match? colors) (span? colors))
            (or (match? shapes) (span? shapes))
            (or (match? fills)  (span? fills)))
       true false)
   )

(define examples (list (list '(r r r) '(m s t) '(f l d))  ;2-span SET
                       (list '(r g r) '(t t s) '(l l l))  ;1-span No-SET
                       (list '(g b r) '(s t s) '(d b l))  ;3-span No-SET
                       (list '(b b b) '(m m m) '(d d d))  ;0-span SET
                       ))

(map SET? examples)