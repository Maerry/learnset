
import math
import rules as rl
import matplotlib.pyplot as plt
import numpy as np
import random as rand
import pandas as pd
import itertools


rule_names = ['A', 'B', 'C', 'D', 'E']
csf = ['Color', 'Shape', 'Fill']


def get_beta_rules(dimension=['r', 'g', 'b'], rule_names=rule_names):
    the_rules = []
    the_rule_names = []
    for rule_name in rule_names:
        the_rules.append(rl.get_all_tuples_for_a_rule(rule_name, dimension))
        the_rule_names.append(rule_name)
    return the_rules, the_rule_names


def get_beta_priors(alphas=pd.DataFrame(data=[[3, 2, 2, 2, 2]], columns=rule_names),
                    betas=pd.DataFrame(data=[np.ones(5)], columns=rule_names)):
    return alphas / (alphas + betas)


def get_alpha_and_beta(trials,
                       rules,
                       dim,
                       alpha_col,
                       beta_col,
                       initial_alpha=np.array([3, 2, 2, 2, 2]),
                       initial_beta=np.array([3, 2, 2, 2, 2])):
    trials[alpha_col] = 'NaN'
    trials[beta_col] = 'NaN'
    if dim in csf:
        initial_alpha = initial_alpha
        initial_beta = initial_beta
    elif len(dim) == 2:
        initial_alpha = np.ones([5, 5])
        initial_beta = np.ones([5, 5])
    elif len(dim) == 3:
        initial_alpha = np.ones([5, 5, 5])
        initial_beta = np.ones([5, 5, 5])
    trials.set_value(0, alpha_col, initial_alpha)
    trials.set_value(0, beta_col, initial_beta)

    for row in range(1, trials.shape[0]):
        new_row_a = trials.loc[row - 1, alpha_col].copy()
        new_row_b = trials.loc[row - 1, beta_col].copy()
        rule_index = rl.get_rule_from_tuple(trials.loc[row, dim], rules, dim)
        if trials.loc[row, 'SET'] == 'SET':
            if not isinstance(rule_index, list):
                new_row_a[rule_index] += 1
            elif len(rule_index) == 2:
                new_row_a[rule_index[0]][rule_index[1]] += 1
            elif len(rule_index) == 3:
                new_row_a[rule_index[0]][rule_index[1]][rule_index[2]] += 1
        else:
            if not isinstance(rule_index, list):
                new_row_b[rule_index] += 1
            elif len(rule_index) == 2:
                new_row_b[rule_index[0]][rule_index[1]] += 1
            elif len(rule_index) == 3:
                new_row_b[rule_index[0]][rule_index[1]][rule_index[2]] += 1
        trials.set_value(row, alpha_col, new_row_a)
        trials.set_value(row, beta_col, new_row_b)

def simulate_random_data(all_tuples, column_names=csf, n_rows = np.arange(250)):
    trials = pd.DataFrame(columns=column_names, index=n_rows)
    # Fill up the data frame with noSET trials
    trials['SET'] = 'noSET'
    for row in range(trials.shape[0]):
        n_rule_breakers = np.random.choice(range(1, 4))
        rule_breaking_dim = np.random.choice(csf, n_rule_breakers, replace=False)
        for dim in csf:
            if dim in rule_breaking_dim:
                tuples = all_tuples.loc[all_tuples['SET'] == 'noSET', dim]
            else:
                tuples = all_tuples.loc[all_tuples['SET'] == 'SET', dim]
            trials.loc[row, dim] = np.random.choice(tuples, 1)[0]
    # Overwrite half of the trials with SET trials
    SET_trials = np.random.choice(range(trials.shape[0]), int(trials.shape[0]/2), replace=False)
    trials.loc[SET_trials, 'SET'] = 'SET'
    for dim in csf:
        SET_tuples = all_tuples.loc[all_tuples['SET'] == 'SET', dim]
        trials.loc[trials['SET'] == 'SET', dim] = np.random.choice(SET_tuples, len(SET_trials))
    return trials

def get_alpha(lower_bound, upper_bound, mean, sd):
    return max(lower_bound, min(upper_bound, round(np.random.normal(mean, sd))))
