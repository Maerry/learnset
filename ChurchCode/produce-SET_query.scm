(define (produce-SET)
  (rejection-query
   
   ;; Rules
   (define (get-color) (if (flip (/ 1 3)) 'r (if (flip 0.5) 'g 'b)))
   (define (get-shape) (if (flip (/ 1 3)) 'm (if (flip 0.5) 's 't)))
   (define (get-fill)  (if (flip (/ 1 3)) 'f (if (flip 0.5) 'l 'd)))
   
   (define colors (list (get-color) (get-color) (get-color)))
   (define shapes (list (get-shape) (get-shape) (get-shape)))
   (define fills  (list (get-fill)  (get-fill)  (get-fill)))
   
   (define (match? feature) (if (and (equal? (first feature) (second feature))
                                     (equal? (second feature) (third feature)))
                                true false))
   (define (span? feature) (if (and (not (equal? (first feature) (second feature)))
                                    (not (equal? (first feature) (third feature)))
                                    (not (equal? (second feature) (third feature))))
                               true false))
  
   (define example (list colors shapes fills))
   
   ;; What we want to know
   example
   
   ;; Condition
   (and (or (match? colors) (span? colors))
        (or (match? shapes) (span? shapes))
        (or (match? fills)  (span? fills)))
   ))

(repeat 10 produce-SET)