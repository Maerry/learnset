
import math
import rules as rl
import matplotlib.pyplot as plt
import numpy as np
import random as rand
import pandas as pd
import itertools


# Switches
selected_rules = 'relational'  # can be 'relational', 'all', 'interesting'
prior_calculation = 'boxes'  # can be 'boxes', 'uniform',


# Rules A ('match'), B ('span'), C ('1==2 & 2!=3), D (1==3&2!=3), E (2==3&1!=2)
# Define rules
rule_names = ['A', 'B', 'C', 'D', 'E']
colors = ['r', 'g', 'b']
n_boxes_system = 3 ** 3
all_tuples = rl.get_all_tuples(colors)
SET_rule = rl.get_all_tuples_for_a_rule('A') + rl.get_all_tuples_for_a_rule('B')

# Part 1: Get initial priors for relational rules
the_rules = []
the_rule_names = []
for rule_name in rule_names:
    the_rules.append(rl.get_all_tuples_for_a_rule(rule_name, colors))
    the_rule_names.append(rule_name)
# for two_rule_names in itertools.combinations(rule_names, 2):
#     the_rules.append(rl.get_all_tuples_for_a_rule(two_rule_names[0]) +
#                              rl.get_all_tuples_for_a_rule(two_rule_names[1]))
#     the_rule_names.append(two_rule_names)
# for three_rule_names in itertools.combinations(rule_names, 3):
#     the_rules.append(rl.get_all_tuples_for_a_rule(three_rule_names[0]) +
#                              rl.get_all_tuples_for_a_rule(three_rule_names[1]) +
#                              rl.get_all_tuples_for_a_rule(three_rule_names[2]))
#     the_rule_names.append(three_rule_names)
# for four_rule_names in itertools.combinations(rule_names, 4):
#     the_rules.append(rl.get_all_tuples_for_a_rule(four_rule_names[0]) +
#                              rl.get_all_tuples_for_a_rule(four_rule_names[1]) +
#                              rl.get_all_tuples_for_a_rule(four_rule_names[2]) +
#                              rl.get_all_tuples_for_a_rule(four_rule_names[3]))
#     the_rule_names.append(four_rule_names)

# C: For some interesting rules
# Show priors for rules A, B, C, D, E; SET_rule; 'all items are blue'; 'the 1st item is red'; '2 items are red'
# Get the tuples for these rules
# the_rule_names = ('SET', 'Match', 'Span', 'C', 'D', 'E', 'all red', 'first red', 'all b or 2 red')
# the_rules = (rl.get_all_tuples_for_a_rule('A') + rl.get_all_tuples_for_a_rule('B'),
#                         rl.get_all_tuples_for_a_rule('A'), rl.get_all_tuples_for_a_rule('B'),
#                         rl.get_all_tuples_for_a_rule('C'), rl.get_all_tuples_for_a_rule('D'),
#                         rl.get_all_tuples_for_a_rule('E'),
#                         (('r', 'r', 'r'),),
#                         (('r', 'r', 'r'), ('r', 'r', 'b'), ('r', 'r', 'g'),
#                          ('r', 'g', 'r'), ('r', 'g', 'b'), ('r', 'g', 'g'),
#                          ('r', 'b', 'r'), ('r', 'b', 'b'), ('r', 'b', 'g')),
#                         (('b', 'b', 'b'),
#                          ('r', 'r', 'b'), ('r', 'r', 'g'),
#                          ('r', 'g', 'r'), ('r', 'b', 'r'),
#                          ('g', 'r', 'r'), ('b', 'r', 'r')))


# 2. Calculate priors for these rules
# - Using size of rule (bad) or uniform (bad)
# rule_priors = [1/len(rule_priors) for i in range(len(rule_priors))]
# example_rule_lengths = [len(example_rules_tuple) for example_rules_tuple in the_rules]
# pre_rule_priors = np.power(math.e, example_rule_lengths)
# rule_priors = [pre_example_prior / np.sum(pre_rule_priors) for pre_example_prior in pre_rule_priors]
# the_rules = [rl.binary_into_tuple(i) for i in range(1, 2**15)]
# example_rule_lengths = [len(example_rules_tuple) for example_rules_tuple in the_rules]
# pre_rule_priors = np.power(math.e, example_rule_lengths)
# rule_priors = [pre_example_prior / np.sum(pre_rule_priors) for pre_example_prior in pre_rule_priors]
# the_rule_names = [i for i in range(1, 2 ** 5)]
# - Using a uniform prior
# norm_rule_priors = np.array([1/5, 1/5, 1/5, 1/5, 1/5])
# norm_rule_priors = np.ones(len(norm_rule_priors)) / len(norm_rule_priors)
# - Using some kind of box complexity
# rule_priors = (np.array([rl.calculate_prior(tpl) for tpl in the_rules]))
# norm_rule_priors = rule_priors / np.sum(rule_priors)
# - Using some kind of beta distribution
alphas = pd.DataFrame(data=[[3, 2, 2, 2, 2]], columns=rule_names)
betas = pd.DataFrame(data=[np.ones(5)], columns=rule_names)
norm_rule_priors = alphas / (alphas + betas)


# 3. Find priors that are wrong
# bugged_priors = [i for i in range(len(priors)) if priors[i] >= 1]
# bugged_tuples = list()
# for bug in bugged_priors:
#     bugged_tuple = rl.binary_into_tuple(bug)
#     bugged_tuples.append(bugged_tuple)
#     n_boxes = rl.get_n_boxes_for_tuples(bugged_tuple)
#     prior = rl.prob_and_comp_from_n_boxes(n_boxes)
#     print(bugged_tuple, '\n', n_boxes, '\n', prior)

# normalize priors
# norm_priors = priors / np.sum(priors)

# Transform rule tuples into binary numbers (to use the priors vector)
# the_rules_binary = [rl.tuple_into_binary(tpl) for tpl in the_rules]
# Retrieve priors from priors vector (not working)
# rule_priors = [priors[i] for i in the_rules_binary]

# Plot the priors
fig2 = plt.figure()
last_index = min(50, len(rule_names))
index = np.arange(last_index)
bar_width = 0.7
opacity = 0.4
plt.bar(index + 0.35, (norm_rule_priors[:last_index]), bar_width)
plt.xlabel('Rules')
plt.ylabel('probability')
plt.xticks(index + bar_width, rule_names[:last_index])
plt.tight_layout()
plt.show()


# Part 2: Calculate likelihoods and posteriors, given data
acceptance = 0.6

# Data
trials = [('r', 'r', 'r'), ('r', 'g', 'r'), ('g', 'b', 'g'), ('r', 'g', 'b'), ('g', 'g', 'r'), ('b', 'r', 'r'),
          ('g', 'g', 'g'), ('g', 'b', 'g'), ('g', 'b', 'r'), ('g', 'b', 'b'),
          ('b', 'b', 'b'), ('b', 'b', 'r'), ('r', 'b', 'g'), ('r', 'g', 'g'),
          ('b', 'b', 'b'), ('r', 'r', 'g'), ('b', 'r', 'g'), ('b', 'r', 'b'),
          ('g', 'g', 'g'), ('r', 'g', 'r'), ('g', 'r', 'b'), ('g', 'b', 'g'),
          ('r', 'r', 'r'), ('r', 'r', 'g'), ('b', 'g', 'r'), ('b', 'r', 'b'),
          ('r', 'r', 'r'), ('r', 'g', 'r'), ('r', 'g', 'b'), ('g', 'g', 'r'),
          ('g', 'g', 'g'), ('g', 'b', 'g'), ('g', 'b', 'r'), ('g', 'b', 'b'),
          ('b', 'b', 'b'), ('g', 'g', 'r'), ('r', 'b', 'g'), ('r', 'g', 'g'),
          ('b', 'b', 'b'), ('r', 'r', 'g'), ('b', 'r', 'g'), ('b', 'r', 'b'),
          ('g', 'g', 'g'), ('r', 'g', 'r'), ('g', 'r', 'b'), ('g', 'b', 'g'),
          ('r', 'r', 'r'), ('r', 'r', 'g'), ('b', 'g', 'r'), ('b', 'r', 'b')]
rand.shuffle(trials)
labels = ['SET' if tpl in SET_rule else 'noSET' for tpl in trials]

# Show likelihoods for these rules
rule_likelihoods = [rl.get_likelihood_single_trial(example_rules_tuple, trials[0], labels[0], acceptance)[0]
                    for example_rules_tuple in the_rules]

fig3 = plt.figure()
last_index = min(50, len(rule_likelihoods))
index = np.arange(len(rule_likelihoods[:last_index]))
bar_width = 0.7
plt.bar(index + 0.35, (rule_likelihoods[:last_index]), bar_width)
plt.ylabel('likelihoods')
plt.xlabel(['Trial', trials[0]])
plt.xticks(index + bar_width, the_rule_names[:last_index])
plt.tight_layout()
plt.show()

# Update priors to get posteriors
# rule_posteriors = np.zeros([len(trials) + 1, len(rule_priors)])
# rule_posteriors[0, :] = norm_rule_priors
# for trial_counter, (trial, label) in enumerate(zip(trials, labels)):
#     rule_likelihoods = [rl.get_likelihood_single_trial(rule_tuples, trial, label, acceptance)[0]
#                         for rule_tuples in the_rules]
#     anti_likelihoods = [rl.get_likelihood_single_trial([tpl for tpl in all_tuples if tpl not in rule_tuples],
#                                                        trial, label, acceptance)[0] for rule_tuples in the_rules]
#     # posteriors = np.array(rule_likelihoods) * rule_posteriors[trial_counter, :] / (np.array(rule_likelihoods) + 1/27)
#     posteriors = np.array(rule_likelihoods) * rule_posteriors[trial_counter, :]
#     rule_posteriors[trial_counter + 1, :] = posteriors / np.sum(posteriors)  # P(trial) = 1/27

# fig4 = plt.figure()
# for counter, rule_column in enumerate(range(rule_posteriors.shape[1])):
#     if counter < 25:
#         posterior = rule_posteriors[:, rule_column]
#         subfig = fig4.add_subplot(5, 5, counter + 1)
#         time_steps = range(len(posterior))
#         subfig.plot(time_steps, posterior)
#         subfig.set_title(the_rule_names[counter])

rule_posteriors = norm_rule_priors
for trial_counter, (trial, label) in enumerate(zip(trials, labels)):
    for rule, rule_name in zip(the_rules, rule_names):
        if trial in rule and label == "SET":
            alphas.loc[:, rule_name] += 1
        elif trial in rule and label == "noSET":
            betas.loc[:, rule_name] += 1
    rule_posteriors = rule_posteriors.append(alphas / (alphas + betas))

# Plot posteriors
for counter, rule in enumerate(rule_names):
    posterior = rule_posteriors.loc[:, rule]
    ax = plt.subplot(2, 3, counter + 1)
    time_steps = range(len(posterior))
    ax.set_ylim([0, 1])
    ax.plot(time_steps, posterior)
    ax.set_title(the_rule_names[counter])


# Step 4: Simulate task behavior with win-stay-loose-sample (WSLS)
stickiness = 0.5
n_simulations = 50
simulation_columns = ['SimulationID', 'TrialID', 'Trial', 'Hypothesis', 'Prediction', 'Likelihood', 'ACC']
simulations = pd.DataFrame(columns=simulation_columns)
# Create a few simulated runs of the task
for simulation in range(n_simulations):
    rule_hypothesis = np.random.choice(the_rules, p=rule_posteriors[0, :])
    for n_trial, (trial, label) in enumerate(zip(trials, labels)):
        # What's the agent's prediction?
        prediction = trial in rule_hypothesis
        # What's the correct answer?
        true_answer = trial in SET_rule
        # Did the agent get it right?
        accuracy = [1 if prediction == true_answer else 0][0]
        for i, rule in enumerate(the_rules):
            if rule == rule_hypothesis:
                hypothesis_name = the_rule_names[i]
                break
        # What's the likelihood of observing the current trial under the current hypothesis?
        likelihood = rl.get_likelihood_single_trial(rule_hypothesis, trial, label, acceptance)[0]
        # Save the current trial to simulations data frame
        new_row = pd.DataFrame(data=[[simulation, n_trial, ''.join(trial), ''.join(hypothesis_name),
                                      prediction, likelihood, accuracy]],
                               columns=simulation_columns)
        simulations = simulations.append(new_row)
        # Decide whether to switch to a new hypothesis
        if rand.random() > math.e ** (-likelihood / stickiness):
            rule_hypothesis = np.random.choice(the_rules, p=rule_posteriors[n_trial + 1, :])

# Average ACCs
accs = pd.DataFrame(columns=range(n_simulations))
for sim_counter, simulation in enumerate(range(n_simulations)):
    a = simulations.loc[simulations['SimulationID'] == simulation, 'ACC']
    accs.loc[:,sim_counter] = a
accs.mean()

# Plot average accuracy for each simulation
fig5 = plt.figure()
index = np.arange(len(accs.mean()))
bar_width = 0.7
plt.bar(index + 0.35, (accs.mean()), bar_width)
plt.ylabel('av. accuracy')
plt.xlabel('simulation ID')
plt.xticks(index + bar_width, index)
plt.tight_layout()
plt.show()

# Plot learning (av. accuracy by trial)
fig6 = plt.figure()
plt.plot(range(len(accs.mean(1))), accs.mean(1))
plt.show()


