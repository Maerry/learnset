from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

ext_modules = [Extension("rules", ["rules.py"])]

setup(
  name = 'stupid little app',
  cmdclass = {'build_ext': build_ext},
  ext_modules = ext_modules
)
