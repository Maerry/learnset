;;;;;;;;;;;;;;;;;;;;;;
;; HELPER FUNCTIONS ;;
;;;;;;;;;;;;;;;;;;;;;;

(define (in-list? list object)
	(if (null? list)
		false
        (if (equal? (first list) object)
            true
            (in-list? (rest list) object))))

(define (flatten l)
    (if (null? l)
        '()
        (if (list? (first l))
            (append (flatten (first l)) (flatten (rest l)))
            (pair (first l) (flatten (rest l))))))

(define (terminal? t) (in-list? terminals t))


;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; MY "SET" RULE GRAMMAR ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define terminals '(first second third features equal? not and or))

(define (transition nonterminal)
    (case nonterminal
        ; R (= rule) is the start symbol. A rule can either be a combination of two rules (ao R R), or an equality evaluation (EQ)
        (('R) (multinomial (list '(ao R R) '(EQ))
                           (list  (/ 1 2)   (/ 1 2))))
        
        ; EQ (= equality evaluation). Tests if two features (F) are the same / are different
        (('EQ) (multinomial (list '(eq F F))
                            (list  1)))
        
        ; F (= feature). Features (possible values: red green blue) are terminal. Each feature of a seen item has the same prob of entering the rule.
        (('F) (multinomial (list '(first) '(second) '(third))
                           (list  (/ 1 3)           (/ 1 3)            (/ 1 3))))
        
        ; eq (equal?) and ao (and / or)
        (('eq) (multinomial (list '(equal?) '(nequal?))
                            (list  (/ 1 2)   (/ 1 2))))
        (('ao) (multinomial (list '(and)  '(or))
                            (list  (/ 1 2) (/ 1 2))))
        
        ; unknown symbol
        (else 'error)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;		
;; FRAGMENT GRAMMAR FUNCTIONS ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; expand-child? flips a coin to decide whether a symbol will be expanded or not
;
(define (expand-child? child)
    (if (terminal? child)
        false
        (flip)))

; two-stage-unfold takes a symbol ('F, 'EQ, ...) and returns it if it is a terminal. If it is
; not a terminal, it applies sample-lexical-item (which unfolds the symbol one or more steps).
; It then applies itself to each returned element, returning terminals and using sample-lexical-item
; to further unfold nonterminals, until the input symbol is completely unfolded into terminals.
;
(define two-stage-unfold
    (lambda (symbol)
        (if (terminal? symbol)
            symbol
            (flatten (map two-stage-unfold (sample-lexical-item symbol))))))

; sample-lexical-item takes a symbol ('F, 'EQ, ...) and unfolds it one step (with 'transition symbol'),
; producing the next symbol(s) down the grammar. It then flips a coin for each resulting symbol to
; decide whether (1) to unfold it too (re-applying two-stage-unfold and itself) or (2) to keep the
; element as it is. This process is repeated until there are only terminals (which cannot be expanded)
; or nonterminals that have been decided not to be expanded. A mixed list of terminals and nonterminals
; is the result.
; This whole procedure is DPmem'd, meaning that the next time sample-lexical-item receives the same input
; symbol, it might recall the previous mixed list of terminals and nonterminals instead of calculating it.
;
(define sample-lexical-item
  (DPmem 10.0
    (lambda (symbol)
        (flatten
            ; this map takes each element of an unfolded symbol and either returns it or applies two-stage-unfold
            (map
                ; lambda function takes a symbol and flips a coin to decide whether to unfold it or not
                (lambda (child)
                    (if (expand-child? child)
                        (two-stage-unfold child)
                        child))
                    ; transition takes a symbol ('F, 'EQ, 'R; 'ao, 'eq) and unfolds it
                    (transition symbol))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PRODUCE 10 EXAMPLE RULES FROM THE GRAMMAR ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (produce-rule) (two-stage-unfold 'R))
(repeat 10 produce-rule)

;; returns, e.g., ((equal? second features third features) (and not equal? second features first features not equal? second features first features) (not equal? second features first features) (not equal? third features second features) (and not equal? second features first features or not equal? second features third features equal? first features second features) (and not equal? second features first features not equal? second features second features) (or or not equal? second features third features equal? second features second features and not equal? second features first features not equal? second features first features))
;; meaning, ((equal? (second features) (third features)) (and (not (equal? (second features) (first features))) (not (equal? (second features) (first features)))) ...


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; FIND A RULE THAT FITS TO SEEN EXAMPLES ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define seen-examples (list '(red red red) '(red blue green) '(blue green red) '(green green green)    ; labelled as "SET"
							'(red red blue) '(blue green green) '(red green red) '(green blue blue)))  ; labelled as "NON-SET"

(define fitting-rule 
	(mh-query 200 100
	
		(define rule (two-stage-unfold 'R))
		
		rule
		
		; missing: check that the rule agrees with the labelled examples (not working because grammar-produced rules are not usable...)
		;(equal? (rule-parser rule (first seen-examples))   'true)
		;(equal? (rule-parser rule (second seen-examples))  'true)
		;(equal? (rule-parser rule (third seen-examples))   'true)
		;(equal? (rule-parser rule (fourth seen-examples))  'true)
		;(equal? (rule-parser rule (fifth seen-examples))   'false)
		;(equal? (rule-parser rule (sixth seen-examples))   'false)
		;(equal? (rule-parser rule (seventh seen-examples)) 'false)
		;(equal? (rule-parser rule (eighth seen-examples))  'false)
		))


(define (in-list? list object)
	(if (null? list)
		false
        (if (equal? (first list) object)
            true
            (in-list? (rest list) object))))

(define (tf? x) (in-list? '(#t #f) x))

(define feature-evaluater
  (lambda (list-rule seen-example)
    (if (equal? list-rule '())
        '()
        
        (case (first list-rule)
         (('first)  (append (list (first seen-example))  (feature-evaluater (rest list-rule) seen-example)))
	     (('second) (append (list (second seen-example)) (feature-evaluater (rest list-rule) seen-example)))
	 	 (('third)  (append (list (third seen-example))  (feature-evaluater (rest list-rule) seen-example)))

		 (else (append (list (first list-rule)) (feature-evaluater (rest list-rule) seen-example)))))))

(feature-evaluater '(not equal? first second) '(red blue green))

(define equal-evaluater
  (lambda (list-rule)
    (if (equal? list-rule '())
        '()
        
        (case (first list-rule)
              (('equal?) (append (list (equal? (second list-rule) (third list-rule)))
                                 (equal-evaluater (rest (rest (rest list-rule))))))
              (('nequal?) (append (list (not (equal? (second list-rule) (third list-rule))))
                                 (equal-evaluater (rest (rest (rest list-rule))))))
              (else (append (list (first list-rule)) (equal-evaluater (rest list-rule))))))))

(equal-evaluater (feature-evaluater '(not equal? first second) '(red red green)))

(define ao-evaluater
  (lambda (list-rule)
    (if (< (length list-rule) 2)
        list-rule
        
        (if (and (tf? (second list-rule)) (tf? (third list-rule)))
            (case (first list-rule)
                  (('and) (append (list (and (second list-rule) (third list-rule)))
                                        (rest (rest (rest list-rule)))))
                  (('or)  (append (list (or  (second list-rule) (third list-rule)))
                                        (rest (rest (rest list-rule))))))
            
            (append (list (first list-rule)) (ao-evaluater (rest list-rule)))))))

(ao-evaluater '(or and #t #t and #t #t))

(define ao-evaluater-wrapper
  (lambda (list-rule)
    (if (< (length list-rule) 2)
        list-rule
        (ao-evaluater-wrapper (ao-evaluater list-rule)))))

(ao-evaluater-wrapper '(and and and #t #t or #t #t or and #t #t and #t #t))
(ao-evaluater-wrapper '(and and and #f #t or #t #t or and #t #t and #t #t))

(define rule-parser
  (lambda (list-rule seen-example)
    (ao-evaluater-wrapper (ao-evaluater (equal-evaluater (feature-evaluater list-rule seen-example))))))

(ao-evaluater-wrapper (ao-evaluater (equal-evaluater (feature-evaluater '(or or equal? third third and or or or equal? first second equal? third third
                  nequal? third second equal? second third nequal? first first equal? first second)
                   '(red blue green)))))
(rule-parser '(or or equal? third third and or or or equal? first second equal? third third
                  nequal? third second equal? second third nequal? first first equal? first second)
                   '(red blue green))

(rule-parser '(or and equal? first second equal? second third  ; SET
                  and and nequal? first second nequal? second third nequal? third first)  ;No-SET
             '(red red red))