
import ray
import rules
import numpy as np

ray.init(start_ray_local=True, num_workers=10)

@ray.remote
def calc_partition(lst):
    A = np.zeros(len(lst))
    for (i, l) in enumerate(lst):
        R = rules.binary_into_tuple(l)
        A[i] = rules.calculate_prior(R)
    return A

r = range(2**27)
list_of_groups = zip(*(iter(r),) * 1000000)
l = [calc_partition.remote(l) for l in list_of_groups]
