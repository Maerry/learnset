# from __future__ import division
import itertools
import math
import numpy as np
from scipy import special


# Part 1: Define my data: tuples and rules
rule_coordinates = dict(A=[1, 1, 1], B=[0, 0, 0], C=[1, 0, 0], D=[0, 0, 1], E=[0, 1, 0])
colors = ['r', 'g', 'b']


# Define tuples (by coordinates in the "tuple space": axis1 = color of item1, axis2 = color of item2, etc.)
def get_all_tuples(colors):
    tuple_coordinates = []
    for color_combos in itertools.combinations_with_replacement(colors, 3):
        for color_permutation in itertools.permutations(color_combos):
            if color_permutation not in tuple_coordinates:
                tuple_coordinates.append(color_permutation)
    return tuple_coordinates
# # test:
# get_all_tuples(colors)


# Functions that translate rules from tuple language into binary language and the reverse
def tuple_into_binary(rule_tuples, colors=['r', 'g', 'b']):
    all_tuples = get_all_tuples(colors)
    zeros_and_ones = [int(tup in rule_tuples) for tup in all_tuples]
    binary = sum([zeros_and_ones[i] * 2 ** i for i in range(len(zeros_and_ones))])
    return binary


def binary_into_tuple(binary, colors=['r', 'g', 'b'], n_boxes_system=3 ** 3):
    potences_of_two = [2 ** i for i in range(n_boxes_system)]
    potences_of_two.reverse()
    zeros_and_ones = np.zeros(n_boxes_system)
    for counter, pot in enumerate(potences_of_two):
        if binary - pot >= 0:
            binary -= pot
            zeros_and_ones[n_boxes_system - 1 - counter] = 1
    all_tuples = get_all_tuples(colors)
    rule_tuples = [all_tuples[i] for i in range(n_boxes_system) if zeros_and_ones[i] == 1]
    return rule_tuples
# test:
# all_tuples = get_all_tuples(colors)
# binary_into_tuple(tuple_into_binary(all_tuples[:10])[1]) == all_tuples[:10]


# Define a function that checks if a rule applies to a tuple
def check_rule(rule_coordinates, tuple):
    answer = (((list(tuple)[0] == list(tuple)[1]) == rule_coordinates[0]) &
              ((list(tuple)[1] == list(tuple)[2]) == rule_coordinates[1]) &
              ((list(tuple)[0] == list(tuple)[2]) == rule_coordinates[2]))
    return answer
# test:
# check_rule(rule_coordinates['A'], ['r', 'r', 'r'])  # t
# check_rule(rule_coordinates['A'], ['r', 'g', 'b'])  # f
# check_rule(rule_coordinates['B'], ['r', 'g', 'b'])  # t
# check_rule(rule_coordinates['B'], ['r', 'r', 'r'])  # f


def get_rule_from_tuple(tpl, rules, dim, csf=['Color', 'Shape', 'Fill']):
    if dim in csf:
        for rule_index, rule in enumerate(rules[dim]):
            if tpl in rule:
                return rule_index
    else:
        rule_indexes = []
        for dim_i in range(len(dim)):
            for rule_index, rule in enumerate(rules[dim[dim_i]]):
                for tpl_i in tpl:
                    if tpl_i in rule:
                        rule_indexes += [rule_index]
        return rule_indexes


# Other basic stuff
def get_tuples_with_specific_color_somewhere(color, item_position):
    color_tuples = []
    for color_combos in itertools.combinations_with_replacement(colors, 2):
        for color_permutations in itertools.permutations(color_combos):
            if item_position == 0:
                color_combo = ((color,) + color_permutations)
            elif item_position == 1:
                color_combo = ((color_permutations[0],) + (color,) + (color_permutations[1],))
            elif item_position == 2:
                color_combo = (color_permutations + (color,))
            if color_combo not in color_tuples:
                color_tuples.append(color_combo)
    return color_tuples
# test:
# get_tuples_with_specific_color_somewhere('r', 0)
# get_tuples_with_specific_color_somewhere('g', 1)
# get_tuples_with_specific_color_somewhere('b', 2)


# Part 2: Define functions to calculate priors and prior DLs

# Get Shannon complexity for boxes of each size in the tuple space -> determines complexity of each rule
# Get 3d-boxes of all shapes and sizes
possible_lengths = [1, 2, 3]
box_sizes = []
for length_combos in itertools.combinations_with_replacement(possible_lengths, 3):
    for box_size in itertools.permutations(length_combos):
        if box_size not in box_sizes:
            box_sizes.append(box_size)


# Find out how many possible locations there are for each box in the tuple space
def possible_locations(coordinate_system_dimensions, box_dimensions):
    x = box_dimensions[0]
    y = box_dimensions[1]
    z = box_dimensions[2]
    if (x == y) & (y == z):
        poss_locs = ((coordinate_system_dimensions[0] + 1 - x) *
                     (coordinate_system_dimensions[1] + 1 - y) *
                     (coordinate_system_dimensions[2] + 1 - z))
    else:
        poss_locs = ((coordinate_system_dimensions[0] + 1 - x) *
                     (coordinate_system_dimensions[1] + 1 - y) *
                     (coordinate_system_dimensions[2] + 1 - z) * 3)
    return poss_locs
# test:
# possible_locations([3, 3, 3], [1, 2, 3])
# possible_locations([2, 2, 2], [1, 1, 2])


# Determine probability and code length for each box
def get_prob_and_complexity_for_tuple_box(coordinate_system_dimensions, box_dimensions):
    # 4 boxes (1x1x1, 3x1x1, 3x3x1, 3x3x3) because of circular axes
    number_tuple_boxes = 4
    probability = 1 / (number_tuple_boxes * possible_locations(coordinate_system_dimensions, box_dimensions))
    complexity = -math.log(probability, 2)
    return [probability, complexity]
# tests:
# get_prob_and_complexity_for_tuple_box([3, 3, 3], [1, 1, 1])
# get_prob_and_complexity_for_tuple_box([3, 3, 3], [1, 1, 3])
# get_prob_and_complexity_for_tuple_box([3, 3, 3], [1, 3, 3])
# get_prob_and_complexity_for_tuple_box([3, 3, 3], [3, 3, 3])


# Find out how to build each rule out of the given boxes -> calculate Shannon complexity for each rule
# Define a function to get all tuples covered by a rule
def get_all_tuples_for_a_rule(rule_name, colors = ['r', 'g', 'b']):
    tuple_list = []
    tuple_coordinates = get_all_tuples(colors)
    for tuple_coords in tuple_coordinates:
        if rule_name == 'X':
            if (not check_rule(rule_coordinates['A'], tuple_coords) and
                not check_rule(rule_coordinates['B'], tuple_coords)):
                tuple_list.append(tuple_coords)
        elif check_rule(rule_coordinates[rule_name], tuple_coords):
            tuple_list.append(tuple_coords)
    return tuple_list
# test:
# get_all_tuples_for_a_rule('A')
# get_all_tuples_for_a_rule('B')
# get_all_tuples_for_a_rule('C')


# Define a function that finds out if tuples are neighbors in a tuple space with "circular axes". Circular axes mean
# that all levels on an axis are neighbors to each other, i.e., 'r' and 'g' have just the same distance as 'r' and 'b'.
def direct_neighbor(tuple0, tuple1):
    tuple_distance = [tuple0[i] == tuple1[i] for i in range(3)]  # compare tuples on each dimension
    return sum(tuple_distance) == 2  # tuples are direct neigbors when they are the same on exactly 2 dimensions
# test:
# direct_neighbor(['r', 'g', 'b'], ['r', 'r', 'b'])  # t
# direct_neighbor(['g', 'b', 'r'], ['r', 'g', 'b'])  # f  # I could also think about adding a feature that combines


# Helper for neighbors(); checks if any number of 3-tuples contain the same tuple, i.e., overlap at all
def contain_same_tuple(tuples):
    same_tuples = 0
    for i in itertools.combinations(range(len(tuples)), 2):
        same_tuples += sum([tuples[i[0]][j] == tuples[i[1]][l] for j in range(3) for l in range(3)])
    return same_tuples > 0


# Helper for neighbors(); checks if three tuples lie in a line, i.e., are all neighbors of each other
def form_3x1x1(three_tuples):
    return (direct_neighbor(three_tuples[0], three_tuples[1]) &
            direct_neighbor(three_tuples[1], three_tuples[2]) &
            direct_neighbor(three_tuples[0], three_tuples[2]))


# Define a function that finds neighbor-tuples (i.e., creating 3x1x1, 3x3x1, and 3x3x3 boxes) in a group of tuples
def neighbors(tuples, colors=['r', 'g', 'b']):
    neighbors_3x1x1 = []
    neighbors_3x3x1 = []
    neighbors_3x3x3 = []
    # 3x1x1
    for three_tuples in itertools.combinations(tuples, 3):
        if form_3x1x1(three_tuples):
            neighbors_3x1x1.append(three_tuples)
    # 3x3x1
    for nine_tuples in itertools.combinations(neighbors_3x1x1, 3):
        # Throw out nine_tuples with duplicates
        if not contain_same_tuple(nine_tuples):
            # Three 3-tuples are in the same plane (3x3x1), if they form six 3-neighbor-tuples
            neighbor_counter = 0
            for three_tuples in itertools.combinations((nine_tuples[0] + nine_tuples[1] + nine_tuples[2]), 3):
                if form_3x1x1(three_tuples):
                    neighbor_counter += 1
            if neighbor_counter == 6:
                neighbors_3x3x1.append(nine_tuples)
    # 3x3x3
    tuple_coordinates = get_all_tuples(colors)
    if sum([(each_tuple in tuples) for each_tuple in tuple_coordinates]) == len(tuple_coordinates):
        neighbors_3x3x3 = tuple_coordinates
    return [neighbors_3x1x1, neighbors_3x3x1, neighbors_3x3x3]
# test:
# neighbors(get_all_tuples_for_a_rule('A'))
# neighbors(get_all_tuples_for_a_rule('B'))
# neighbors(get_all_tuples_for_a_rule('C'))
# neighbors(get_all_tuples_for_a_rule('C') + get_all_tuples_for_a_rule('D'))
# neighbors(get_all_tuples_for_a_rule('A') + get_all_tuples_for_a_rule('C') + get_all_tuples_for_a_rule('D'))
# neighbors(get_all_tuples_for_a_rule('A') + get_all_tuples_for_a_rule('C') + get_all_tuples_for_a_rule(
#     'D') + get_all_tuples_for_a_rule('E'))
# neighbors(get_all_tuples_for_a_rule('A') + get_all_tuples_for_a_rule('B') + get_all_tuples_for_a_rule(
#     'C') + get_all_tuples_for_a_rule('D') + get_all_tuples_for_a_rule('E'))


# Define function that removes tuples inside neighbors_3x3x1 from neighbors_3x1x1
def remove_331_neighbors_from_311_neighbor_list(neighbors_3x1x1, neighbors_3x3x1_without_duplicates):
    neigh3x1x1_without_neigh3x3x1 = neighbors_3x1x1[:]
    for neig331 in neighbors_3x3x1_without_duplicates:  # go through all 3x3x1 neighbors
        for neigh331 in neig331:
            for neigh311 in neighbors_3x1x1:
                # find 3x1x1 neighbors that are inside 3x3x1 neighbors
                if (neigh311[0] in neigh331) | (neigh311[1] in neigh331) | (neigh311[2] in neigh331):
                    if neigh311 in neigh3x1x1_without_neigh3x3x1:
                        neigh3x1x1_without_neigh3x3x1.remove(neigh311)
    return neigh3x1x1_without_neigh3x3x1


# Define function that checks on which dimension the first tuple of a group is varying
def get_direction_of_slice_or_stick(tuples):
    tuple0 = tuples[0][0]
    tuple1 = tuples[0][1]
    if tuple0[0] != tuple1[0]:
        varying_dimension = 0
    elif tuple0[1] != tuple1[1]:
        varying_dimension = 1
    elif tuple0[2] != tuple1[2]:
        varying_dimension = 2
    return varying_dimension


# Define function that checks how to best construct tuples from boxes
def get_n_boxes_for_tuples(rule_tuples):
    n_tuples = len(rule_tuples)
    neighbors_3x1x1 = neighbors(rule_tuples)[0]
    neighbors_3x3x1 = neighbors(rule_tuples)[1]
    neighbors_3x3x3 = neighbors(rule_tuples)[2]

    # Find largest box (starting at 3x3x1), put neighbors inside, find box for remaining neighbors, put inside, ...
    # 3x3x1 boxes
    if len(neighbors_3x3x1) > 0:
        slice_directions = []
        for neigh in neighbors_3x3x1:
            slice_directions.append(get_direction_of_slice_or_stick(neigh))
        direction_counter = []
        for dim in range(3):
            varies_on_that_dim = [i == dim for i in slice_directions]
            direction_counter.append(sum(varies_on_that_dim))
        most_popular_dim = [dim for dim in range(3) if direction_counter[dim] == max(direction_counter)][0]
        keep_this_tuple = [slice_directions[slice] == most_popular_dim for slice in range(len(slice_directions))]
        neighbors_3x3x1_same_direction = [neighbors_3x3x1[pos] for pos in range(len(neighbors_3x3x1)) if
                                          keep_this_tuple[pos]]
        neighbors_3x3x1_without_overlap = neighbors_3x3x1_same_direction[:]
        while contain_same_tuple(neighbors_3x3x1_without_overlap):
            neighbors_3x3x1_without_overlap = neighbors_3x3x1_without_overlap[0:(len(neighbors_3x3x1_same_direction)-1)]
        number_3x3x1_boxes = len(neighbors_3x3x1_without_overlap)
    else:
        number_3x3x1_boxes = 0
        neighbors_3x3x1_without_overlap = []

    # 3x1x1 boxes
    neigh3x1x1_without_neigh3x3x1 = remove_331_neighbors_from_311_neighbor_list(neighbors_3x1x1,
                                                                                neighbors_3x3x1_without_overlap)
    if len(neigh3x1x1_without_neigh3x3x1) > 0:
        stick_directions = []
        for neigh in neigh3x1x1_without_neigh3x3x1:
            stick_directions.append(get_direction_of_slice_or_stick((neigh, '')))
        direction_counter = []
        for dim in range(3):
            direction_counter.append(sum([i == dim for i in stick_directions]))
        most_popular_dim = [dim for dim in range(3) if direction_counter[dim] == max(direction_counter)][0]
        keep_this_tuple = [stick_directions[slice] == most_popular_dim for slice in range(len(stick_directions))]
        neighbors_3x1x1_same_direction = [neigh3x1x1_without_neigh3x3x1[pos] for pos in
                                          range(len(neigh3x1x1_without_neigh3x3x1)) if keep_this_tuple[pos]]
        number_3x1x1_boxes = len(neighbors_3x1x1_same_direction)
    else:
        number_3x1x1_boxes = 0

    # 3x3x3 box
    if len(neighbors_3x3x3) > 0:
        number_3x3x3_boxes = 1
        number_3x3x1_boxes = 0
        number_3x1x1_boxes = 0
    else:
        number_3x3x3_boxes = 0

    # 1x1x1 boxes
    number_1x1x1_boxes = n_tuples - 3 * number_3x1x1_boxes - 9 * number_3x3x1_boxes - 27 * number_3x3x3_boxes

    box_numbers = [number_1x1x1_boxes, number_3x1x1_boxes, number_3x3x1_boxes, number_3x3x3_boxes]

    # Make sure there are no negative box numbers
    neg_box_numbers = [n < 0 for n in box_numbers]
    if sum(neg_box_numbers) > 0:
        print('Error! Number of boxes < 0!')

    return box_numbers
# test:
# for i in range(1000000):
#     rule_tuples = binary_into_tuple(i)
#     n_boxes = get_n_boxes_for_tuples(rule_tuples)
#     prior = prob_and_comp_from_n_boxes(n_boxes)
#     if n_boxes[2] > 0:
#         print(i, '\n', rule_tuples, '\n', n_boxes, '\n', prior)


# Define a function that gets complexity from boxes
def prob_and_comp_from_n_boxes(n_boxes, system_dims=[3, 3, 3]):
    # Total complexity is sum of complexities of component boxes
    complexity = (n_boxes[0] * get_prob_and_complexity_for_tuple_box(system_dims, [1, 1, 1])[1] +
                  n_boxes[1] * get_prob_and_complexity_for_tuple_box(system_dims, [1, 1, 3])[1] +
                  n_boxes[2] * get_prob_and_complexity_for_tuple_box(system_dims, [1, 3, 3])[1] +
                  n_boxes[3] * get_prob_and_complexity_for_tuple_box(system_dims, [3, 3, 3])[1])

    # Probability is product of probabilities of component boxes(?)
    # probability= (get_prob_and_complexity_for_tuple_box(system_dims, [1, 1, 1])[0] ** n_boxes[0] *
    #               get_prob_and_complexity_for_tuple_box(system_dims, [1, 1, 3])[0] ** n_boxes[1] *
    #               get_prob_and_complexity_for_tuple_box(system_dims, [1, 3, 3])[0] ** n_boxes[2] *
    #               get_prob_and_complexity_for_tuple_box(system_dims, [3, 3, 3])[0] ** n_boxes[3])
    probability = 200-complexity  # math.e ** -complexity

    return [probability, complexity]
# test:
# rule_names = ['A', 'B', 'C', 'D', 'E']
# for rule_name in rule_names:
#     print([rule_name, prob_and_comp_from_n_boxes(get_n_boxes_for_tuples(get_all_tuples_for_a_rule(rule_name)))[1]])


# Part 2: Get likelihoods P(D|R) for all rules (defined by positive examples)
# Define a function that calculates the likelihood of one data example, under a specific rule
def get_likelihood_single_trial(rule_tuples, trial, label, acceptance=0.8, n_boxes_system=3 ** 3):
    
    # Check how likely it is to pick a 1x1x1 box given the rule, and given the whole hypothesis space
    n_boxes_rule = len(rule_tuples)
    prob_1x1x1_system = 1 / n_boxes_system
    prob_1x1x1_rule = 1 / n_boxes_rule
    prob_1x1x1_not_rule = 1 / (n_boxes_system - n_boxes_rule)

    # Likelihood is acceptance-weighted average of probability given rule and probability given whole hypothesis space
    data_tuple_in_rule = trial in rule_tuples
    if label == 'SET':
        likelihood = data_tuple_in_rule * acceptance * prob_1x1x1_rule + (1 - acceptance) * prob_1x1x1_system
    else:
        likelihood = (not data_tuple_in_rule) * acceptance * prob_1x1x1_not_rule + (1 - acceptance) * prob_1x1x1_system

    # Complexity is the negative log of the probability
    likelihood_DL = -np.log(likelihood)

    return [likelihood, likelihood_DL]
# test:
# DL should be huge, likelihood should be small (rule A rule can't describe B examples)
get_likelihood_single_trial(get_all_tuples_for_a_rule('A'), ('g', 'b', 'r'), 'SET')  # low likelihood
# Other way around
get_likelihood_single_trial(get_all_tuples_for_a_rule('A'), ('r', 'r', 'r'), 'SET')  # high likelihood
# Same for rule B
get_likelihood_single_trial(get_all_tuples_for_a_rule('B'), ('r', 'r', 'r'), 'SET')  # low likelihood
get_likelihood_single_trial(get_all_tuples_for_a_rule('B'), ('g', 'b', 'r'), 'SET')  # high likelihood
# How about NOSETs?
get_likelihood_single_trial(get_all_tuples_for_a_rule('A'), ('g', 'g', 'r'), 'noSET')  # high likelihood
get_likelihood_single_trial(get_all_tuples_for_a_rule('B'), ('r', 'r', 'b'), 'noSET')  # high likelihood
get_likelihood_single_trial(get_all_tuples_for_a_rule('C'), ('g', 'g', 'r'), 'noSET')  # low likelihood


# Part 3: Integrate priors and likelihoods - update the complexity of rules in each trial
# Function that calculates prior DLs
def calculate_prior(rule_tuples):
    n_boxes = get_n_boxes_for_tuples(rule_tuples)
    return prob_and_comp_from_n_boxes(n_boxes)[0]


# Function that calculates posterior DLs
def calculate_posterior(rule_tuples, data, acceptance, n_system_boxes=3 ** 3):
    posteriors = np.zeros([len(data) + 1, 2])
    n_boxes = get_n_boxes_for_tuples(rule_tuples)
    posteriors[0, :] = prob_and_comp_from_n_boxes(n_boxes)
    trial_counter = 1
    for trial, label in data.items():
        if label == 'SET':
            likelihood_prob_comp = get_likelihood_single_trial(trial in rule_tuples, len(rule_tuples), acceptance)
        else:
            likelihood_prob_comp = [0, 0]
        # Update probability (-> posterior probability)
        posterior_rule = posteriors[trial_counter - 1, 0] * likelihood_prob_comp[0]
        posterior_all_rules = 1 / n_system_boxes
        post_probability = posterior_rule / posterior_all_rules
        # Update complexity (-> posterior complexity)
        post_complexity = posteriors[trial_counter - 1, 1] + likelihood_prob_comp[1]
        # post_probability = math.e ** (-post_complexity)
        posteriors[trial_counter, :] = [post_probability, post_complexity]
        trial_counter += 1
    return posteriors
# test:
# data = {('r', 'r', 'r'): 'SET', ('r', 'g', 'b'): 'SET', ('g', 'g', 'g'): 'SET',
#         ('b', 'r', 'g'): 'SET', ('b', 'b', 'b'): 'SET', ('g', 'b', 'r'): 'SET'}
# data = {('r', 'r', 'r'): 'SET', ('g', 'g', 'g'): 'SET', ('b', 'b', 'b'): 'SET'}
# calculate_posterior(get_all_tuples_for_a_rule('B'), data, 0.9)
# calculate_posterior(get_all_tuples_for_a_rule('A'), data, 0.9)

