(define (in-list? list object)
   (if (null? list)
       false
       (if (equal? (first list) object)
           true
           (in-list? (rest list) object))))

(define (flatten l)
  (if (null? l)
      '()
      (if (list? (first l))
          (append (flatten (first l)) (flatten (rest l)))
          (pair (first l) (flatten (rest l))))))


(define terminals '(r g b m s t f l d))

(define (terminal? t) (in-list? terminals t))

(define (transition nonterminal)
  (case nonterminal
        (('color1) (multinomial(list (list 'r))
                               (list (/ 1 1))))
        (('color2) (multinomial(list (list 'g))
                               (list (/ 1 1))))
        (('color3) (multinomial(list (list 'b))
                               (list (/ 1 1))))
        (('shape1) (multinomial(list (list 'm))
                               (list (/ 1 1))))
        (('shape2) (multinomial(list (list 's))
                               (list (/ 1 1))))
        (('shape3) (multinomial(list (list 't))
                               (list (/ 1 1))))
        (('fill1) (multinomial(list (list 'f))
                               (list (/ 1 1))))
        (('fill2) (multinomial(list (list 'l))
                               (list (/ 1 1))))
        (('fill3) (multinomial(list (list 'd))
                               (list (/ 1 1))))
        
        (('match-c) (multinomial (list (list 'color1 'color1 'color1)
                                       (list 'color2 'color2 'color2)
                                       (list 'color3 'color3 'color3))
                                 (list (/ 1 3) (/ 1 3) (/ 1 3))))
        (('span-c)  (multinomial (list (list 'color1 'color2 'color3)
                                       (list 'color1 'color3 'color2)
                                       (list 'color2 'color1 'color3)
                                       (list 'color2 'color3 'color1)
                                       (list 'color3 'color1 'color2)
                                       (list 'color3 'color2 'color3))
                                 (list (/ 1 6) (/ 1 6) (/ 1 6) (/ 1 6) (/ 1 6) (/ 1 6))))
        
        (('match-s) (multinomial (list (list 'shape1 'shape1 'shape1)
                                       (list 'shape2 'shape2 'shape2)
                                       (list 'shape3 'shape3 'shape3))
                                 (list (/ 1 3) (/ 1 3) (/ 1 3))))
        (('span-s)  (multinomial (list (list 'shape1 'shape2 'shape3)
                                       (list 'shape1 'shape3 'shape2)
                                       (list 'shape2 'shape1 'shape3)
                                       (list 'shape2 'shape3 'shape1)
                                       (list 'shape3 'shape1 'shape2)
                                       (list 'shape3 'shape2 'shape3))
                                 (list (/ 1 6) (/ 1 6) (/ 1 6) (/ 1 6) (/ 1 6) (/ 1 6))))
        
        (('match-f) (multinomial (list (list 'fill1 'fill1 'fill1)
                                       (list 'fill2 'fill2 'fill2)
                                       (list 'fill3 'fill3 'fill3))
                                 (list (/ 1 3) (/ 1 3) (/ 1 3))))
        (('span-f)  (multinomial (list (list 'fill1 'fill2 'fill3)
                                       (list 'fill1 'fill3 'fill2)
                                       (list 'fill2 'fill1 'fill3)
                                       (list 'fill2 'fill3 'fill1)
                                       (list 'fill3 'fill1 'fill2)
                                       (list 'fill3 'fill2 'fill3))
                                 (list (/ 1 6) (/ 1 6) (/ 1 6) (/ 1 6) (/ 1 6) (/ 1 6))))
        
        (('SET) (multinomial (list (list 'match-c 'match-s 'match-f)  ;0-span SET
                                   (list 'span-c  'match-s 'match-f)  ;1-span SET
                                   (list 'match-c 'span-s  'match-f)  ;1-span SET
                                   (list 'match-c 'match-s 'span-f)   ;1-span SET
                                   (list 'span-c  'span-s  'match-f)  ;2-span SET
                                   (list 'span-c  'match-s 'span-f)   ;2-span SET
                                   (list 'match-c 'span-s  'span-f)   ;2-span SET
                                   (list 'span-c  'span-s  'span-f))  ;3-span SET
                             (list (/ 1 4)                            ;0-span
                                   (/ 1 12) (/ 1 12) (/ 1 12)         ;1-span
                                   (/ 1 12) (/ 1 12) (/ 1 12)         ;2-span
                                   (/ 1 4))))                         ;3-span
        (else 'error)))

(define (expand-child? child)
  (if (terminal? child)
      false
      (flip)))

(define sample-lexical-item
  (DPmem 10.0
         (lambda (symbol)
           (flatten (map (lambda (child)
                           (if (expand-child? child)
                               (two-stage-unfold child)
                               child))
                         (transition symbol))))))

(define two-stage-unfold
  (lambda (symbol)
    (if (terminal? symbol)
        symbol
        (flatten (map two-stage-unfold  (sample-lexical-item symbol))))))

(repeat 10 (lambda () (two-stage-unfold 'SET)))