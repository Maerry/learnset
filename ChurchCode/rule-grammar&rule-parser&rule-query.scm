;;;;;;;;;;;;;;;;;;;;;;
;; HELPER FUNCTIONS ;;
;;;;;;;;;;;;;;;;;;;;;;

; little functions that make life easier
(define (in-list? list object)
	(if (null? list)
		false
        (if (equal? (first list) object)
            true
            (in-list? (rest list) object))))

(define (flatten l)
    (if (null? l)
        '()
        (if (list? (first l))
            (append (flatten (first l)) (flatten (rest l)))
            (pair (first l) (flatten (rest l))))))

(define (remove item lst)
  (cond ((null? lst)
         '())
        ((equal? item (car lst))
         (cdr lst))
        (else
         (cons (car lst) 
               (remove item (cdr lst))))))

(define shuffle
  (lambda (urn)
    (if (> (length urn) 0)
        (let ((item (uniform-draw urn)))
          (append (list item) (shuffle (remove item urn))))
        (list))))

(define tf-combine
  (lambda (tf-list)
    (if (> (length tf-list) 1)
        (tf-combine (append (list (and (first tf-list) (second tf-list)))
                            (rest (rest tf-list))))
        (first tf-list))))

(define (terminal? t) (in-list? terminals t))

(define (tf? x) (in-list? '(#t #f) x))


;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; MY "SET" RULE GRAMMAR ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;

; defines the form of SET rules; can be used to produce potential SET rules.
(define terminals '(first second third equal? nequal? and or))

(define (transition nonterminal)
    (case nonterminal
        ; S (= SET-rule) is the start symbol. The SET-rule is a rule (R)
        (('S) (multinomial (list '(R))
                           (list  1)))
          
        ; R (= a rule) can either be a combination of two rules (ao R R), or an equality evaluation (EQ)
        (('R) (multinomial (list '(ao R R) '(EQ))       ; this is recursive!! ok?
                           (list  (/ 1 3)   (/ 2 3))))  ; should this be an uninformative prior instead?
        
        ; EQ (= equality evaluation). Tests if two features (F) are the same / are different
        (('EQ) (multinomial (list '(eq FF))
                            (list  1)))
        
        ; F (= feature). Features (possible values: red green blue) are terminal. Each feature of a seen item has the same prob of entering the rule.
        (('FF) (multinomial (list '(first second) '(second third) '(first third))
                            (list  (/ 1 3)         (/ 1 3)         (/ 1 3))))
        
        ; eq (equal?) and ao (and / or)
        (('eq) (multinomial (list '(equal?) '(nequal?))
                            (list  (/ 1 2)   (/ 1 2))))
        (('ao) (multinomial (list '(and)    '(or))
                            (list  (/ 1 2)   (/ 1 2))))
        
        ; unknown symbol
        (else 'error)))

(define crucial-examples (list (list '(red red red) #t)
                               (list '(red blue green) #t)
                               (list '(red red blue) #f)
                               (list '(red blue red) #f)
                               (list '(blue red red) #f)))

(define all-examples
    (list 
          ; SETs
          (list '(red red red) #t)
          (list '(green green green) #t)
          (list '(blue blue blue) #t)
          (list '(red blue green) #t)
          (list '(red green blue) #t)
          (list '(blue red green) #t)
          (list '(blue green red) #t)
          (list '(green red blue) #t)
          (list '(green blue red) #t)
          ; No-SETs
          (list '(red red green) #f)
          (list '(red green red) #f)
          (list '(green red red) #f)
		  (list '(red red blue) #f)
		  (list '(red blue red) #f)
		  (list '(blue red red) #f)
		  (list '(blue blue green) #f)
		  (list '(blue green blue) #f)
		  (list '(green blue blue) #f)
          (list '(blue blue red) #f)
          (list '(blue red blue) #f)
          (list '(red blue blue) #f)
		  (list '(green green red) #f)
		  (list '(green red green) #f)
		  (list '(red green green) #f)
          (list '(green green blue) #f)
          (list '(green blue green) #f)
          (list '(blue green green) #f)))

(define most-examples
    (list 
          ; SETs
          (list '(red red red) #t)
          (list '(green green green) #t)
          (list '(blue blue blue) #t)
          (list '(red blue green) #t)
          (list '(red green blue) #t)
          (list '(blue red green) #t)
          (list '(blue green red) #t)
          (list '(green red blue) #t)
          (list '(green blue red) #t)
          ; No-SETs
          (list '(red red green) #f)
          (list '(red green red) #f)
          (list '(green red red) #f)
		  (list '(blue blue green) #f)
		  (list '(blue green blue) #f)
		  (list '(green blue blue) #f)
          (list '(green green blue) #f)
          (list '(green blue green) #f)
          (list '(blue green green) #f)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;		
;; FRAGMENT GRAMMAR FUNCTIONS ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; come from ...

; expand-child? flips a coin to decide whether a symbol will be expanded or not
;
(define (expand-child? child)
    (if (terminal? child)
        false
        (flip)))

; two-stage-unfold takes a symbol ('F, 'EQ, ...) and returns it if it is a terminal. If it is
; not a terminal, it applies sample-lexical-item (which unfolds the symbol one or more steps).
; It then applies itself to each returned element, returning terminals and using sample-lexical-item
; to further unfold nonterminals, until the input symbol is completely unfolded into terminals.
;
(define two-stage-unfold
    (lambda (symbol)
        (if (terminal? symbol)
            symbol
            (flatten (map two-stage-unfold (sample-lexical-item symbol))))))

; sample-lexical-item takes a symbol ('F, 'EQ, ...) and unfolds it one step (with 'transition symbol'),
; producing the next symbol(s) down the grammar. It then flips a coin for each resulting symbol to
; decide whether (1) to unfold it too (re-applying two-stage-unfold and itself) or (2) to keep the
; element as it is. This process is repeated until there are only terminals (which cannot be expanded)
; or nonterminals that have been decided not to be expanded. A mixed list of terminals and nonterminals
; is the result.
; This whole procedure is DPmem'd, meaning that the next time sample-lexical-item receives the same input
; symbol, it might recall the previous mixed list of terminals and nonterminals instead of calculating it.
;
(define sample-lexical-item
  (DPmem 30
    (lambda (symbol)
        (flatten
            ; this map takes each element of an unfolded symbol and either returns it or applies two-stage-unfold
            (map
                ; lambda function takes a symbol and flips a coin to decide whether to unfold it or not
                (lambda (child)
                    (if (expand-child? child)
                        (two-stage-unfold child)
                        child))
                    ; transition takes a symbol ('F, 'EQ, 'R; 'ao, 'eq) and unfolds it
                    (transition symbol))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PRODUCE 10 EXAMPLE RULES FROM THE GRAMMAR ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (produce-rule) (two-stage-unfold 'S))
(repeat 10 produce-rule)
; RETURNS ((equal? second third) (and nequal? second first nequal? second first) ...)
; meaning, ((equal? (second features) (third features)) (and (not (equal? (second features) (first features))) ...)
; (LISP only produces output if everything after is commented out)
(define rule (produce-rule))
rule


;;;;;;;;;;;;;;;;
;; SET PARSER ;;
;;;;;;;;;;;;;;;;

; The SET parser applies a rule to an example; it takes a rule and an examples and returns #t or #f,
; depending on whether the rule describes the example or not.

; feature-evaluator takes a rule produced by the grammar (called 'list-rule') and a seen example
; and replaces all features mentioned in the rule by the actual features of the seen example
;
(define feature-evaluator
  (lambda (list-rule seen-example)
    (if (equal? list-rule '())
        '()
        
        (case (first list-rule)
         (('first)  (append (list (first seen-example))  (feature-evaluator (rest list-rule) seen-example)))
	     (('second) (append (list (second seen-example)) (feature-evaluator (rest list-rule) seen-example)))
	 	 (('third)  (append (list (third seen-example))  (feature-evaluator (rest list-rule) seen-example)))

		 (else (append (list (first list-rule)) (feature-evaluator (rest list-rule) seen-example)))))))
; test:
(feature-evaluator '(not equal? first second) '(red blue green))

; equal-evaluator takes a rule, in which the features have already been evaluated, and replaces 
; "(equal? F1 F2)" statements with #t or #f, depending on their truth value.
;
(define equal-evaluator
  (lambda (list-rule)
    (if (equal? list-rule '())
        '()
        
        (case (first list-rule)
              (('equal?) (append (list (equal? (second list-rule) (third list-rule)))
                                 (equal-evaluator (rest (rest (rest list-rule))))))
              (('nequal?) (append (list (not (equal? (second list-rule) (third list-rule))))
                                 (equal-evaluator (rest (rest (rest list-rule))))))
              (else (append (list (first list-rule)) (equal-evaluator (rest list-rule))))))))
; test:
(equal-evaluator (feature-evaluator '(not equal? first second) '(red red green)))

; ao-evaluator takes a rule, in which the features and equality statements have already been evaluated,
; and replaces "(and #tf #tf)" and "(or #tf #tf)" statements with #tf
;
(define ao-evaluator
  (lambda (list-rule)
    (if (< (length list-rule) 2)
        list-rule
        
        (if (and (tf? (second list-rule)) (tf? (third list-rule)))
            (case (first list-rule)
                  (('and) (append (list (and (second list-rule) (third list-rule)))
                                        (rest (rest (rest list-rule)))))
                  (('or)  (append (list (or  (second list-rule) (third list-rule)))
                                        (rest (rest (rest list-rule))))))
            
            (append (list (first list-rule)) (ao-evaluator (rest list-rule)))))))
; test:
(ao-evaluator '(or and #t #t and #t #t))

; ao-evaluator-wrapper takes a rule, in which  the features and equality statements have already been
; evaluated, and applies the ao-evaluator repeatedly until all statements have been evaluated and
; only a single instance of #tf remains
;
(define ao-evaluator-wrapper
  (lambda (list-rule)
    (if (< (length list-rule) 2)
        list-rule
        (ao-evaluator-wrapper (ao-evaluator list-rule)))))
; test:
(ao-evaluator-wrapper '(and and and #t #t or #t #t or and #t #t and #t #t))    ; correct answer: #t
(ao-evaluator-wrapper '(and and and #f #t or #t #t or and #t #t and #t #t))    ; correct answer: #f

; rule-parser applies the functions feature-evaluator, equal-evaluator, and ao-evaluator(-wrapper)
; to a rule produced by the grammar
;
(define rule-parser
  (lambda (list-rule seen-example)
    (first (ao-evaluator-wrapper (ao-evaluator (equal-evaluator
                                                (feature-evaluator list-rule seen-example)))))))
; test1 (rule generated by grammar):
(rule-parser '(or or equal? third third and or or or equal? first second equal? third third
                  nequal? third second equal? second third nequal? first first equal? first second)
                   '(red blue green))
; test2 (correct rule for SET):
(rule-parser '(or and equal? first second equal? second third  ; SET
                  and and nequal? first second nequal? second third nequal? third first)  ;No-SET
             '(red red red))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; FIND A RULE THAT FITS TO SEEN EXAMPLES ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Helper functions
(define test-rule
	(lambda (rule example label)
		(equal? (rule-parser rule example) label)))

(define test-rule-map
	(lambda (rule examples labels)
		(if (> (length examples) 0)
			(append (list (test-rule rule (first examples) (first labels)))
                    (test-rule-map rule (rest examples) (rest labels)))
			'())))

(define get-examples
  (lambda (exlabs)
    (if (> (length exlabs) 0)
      (append (list (first (first exlabs))) (get-examples (rest exlabs)))
      (list))))

(define get-labels
  (lambda (exlabs)
    (if (> (length exlabs) 0)
      (append (list (second (first exlabs))) (get-labels (rest exlabs)))
      (list))))

;tests:
(test-rule '(equal? first second) '(red red red) #t)
(test-rule-map '(equal? first second)
               (list '(red red red) '(red green blue) '(red red green) '(blue green blue))
               (list #t #t #f #f))
(tf-combine (list #t #t #t #t #t #t #t #t))   ; should return #t
(tf-combine (list #f #t #t #t #t #t #t #t))   ; should return #f

;; Define find-rule
(define find-rule
	(lambda (seen-examples seen-labels)
;;		(mh-query 1 1
		(rejection-query
			(define rule (two-stage-unfold 'S))
			rule
			(condition (tf-combine (test-rule-map rule seen-examples seen-labels))))))
; test find-rule:
;; (find-rule (list '(red red red) '(green green green) '(green green red) '(red blue green) '(red blue red))
;;            '(#t #t #f #t #f))
; RETURNS (or and nequal? first second nequal? first third equal? second third)
;; (find-rule (list '(red red red) '(red blue green) '(red red blue) '(red blue red) '(blue red red))
;;            '(#t #t #f #f #f))
; RETURNS (WFT?!) (and or equal? first second nequal? second third or and nequal? first third or nequal? first second equal? second third equal? second third)
; i.e, ((e12 o n23) a ((n13 a (n12 o e23)) o e23))


;;;;;;;;;;;;;;;;;;;;;;;;;
;; "DO THE EXPERIMENT" ;;
;;;;;;;;;;;;;;;;;;;;;;;;;

; find a rule based on (a few) already-seen examples and use this rule to predict the next example.
; MISSING: integrate the feedback about whether the prediction was right or wrong! Try to maximize correct predictions!

;; Define find-rule-and-predict
; find-rule-and-predict takes the list of examples that will be shown in the experiment, together with the correct labels
; and one starting examples (should be an empty list -> TD!). It goes through the examples sequentially, like in the real
; experiment; it starts by finding a rule that agrees with (a subset of) the already-seen examples; it then uses this rule
; to make a guess about the next example.
; The function outputs a list, in which the following information is listed for each trial:
; 1) What are the (randomly chosen) "OLD" examples, on which the "OLD" rule is based?
; 2) What is the "OLD" rule?
; 3) What "NEW" example was shown in this trial?
; 4) Does the "OLD" rule classify the "NEW" example correctly?
;
(define find-rule-and-predict
  (lambda (exlabs seen-exlabs)
    (if (> (length exlabs) 0)
        (let ((trial-exlabs (take (shuffle seen-exlabs) 3)))
          (let ((rule (find-rule (get-examples trial-exlabs) (get-labels trial-exlabs))))
            (let ((prediction (test-rule rule (first (get-examples (list (first exlabs))))
                                              (first (get-labels   (list (first exlabs)))))))
              (append (list
                       ;; what are the (randomly chosen) "OLD" examples, on which the "OLD" rule is based?
                       (get-examples trial-exlabs)
                       ;; what is the "OLD" rule?
                       rule
                       ;; what "NEW" example was shown in this trial?
                       (get-examples (list (first exlabs)))
                       ;; does the "OLD" rule classify the "NEW" example correctly?
                       prediction
                       'NEXTTRIAL)
                      
                      (find-rule-and-predict (rest exlabs) (append (list (first exlabs)) seen-exlabs))))))
        (list))))

; test:
; Define function input and a (random) starting rule
;; (define exlabs (append (shuffle most-examples) (shuffle most-examples)))
(define exlabs (shuffle most-examples))
(define rule (produce-rule))
(define seen-exlabs (list (list '(blue red red) #f)))

; apply find-rule-and-predict
(find-rule-and-predict exlabs seen-exlabs)
; RETURNS (((blue red red)) (or and equal? first second equal? first third equal? first second) ((green green green)) #t NEXTTRIAL ((blue red red) (green green green)) (equal? first second) ((blue blue red)) #f NEXTTRIAL ((green green green) (blue blue red) (blue red red)) (equal? first third) ((green green blue)) #t NEXTTRIAL ((green green blue) (green green green) (blue red red)) (equal? first third) ((green blue red)) #f NEXTTRIAL ((blue red red) (green green blue) (green green green)) (equal? first third) ((green red blue)) #f NEXTTRIAL ((green blue red) (blue red red) (blue blue red)) (and nequal? first second or nequal? second third nequal? second third) ((red red red)) #f NEXTTRIAL ((green green green) (green blue red) (blue red red)) (or nequal? second third equal? first third) ((blue green green)) #t NEXTTRIAL ((green green blue) (blue green green) (green blue red)) (and nequal? second third nequal? first second) ((blue red green)) #t ....
